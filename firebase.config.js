import { initializeApp } from 'firebase/app';
import { getFirestore, query, collection, orderBy } from 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyCqK0UKHjqvttmZzIatEjLjfjX8P2gg1tE',
  authDomain: 'projectname-xxx.firebaseapp.com',
  projectId: 'projectname-xxx',
  storageBucket: 'projectname-xxx.appspot.com',
  messagingSenderId: '323589431735',
  appId: '1:323589431735:web:161e00e29236e68f578832',
};
export const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const places = query(collection(db, 'places'), orderBy('value', 'desc'));
export const payments = query(
  collection(db, 'payment'),
  orderBy('added', 'desc')
);
