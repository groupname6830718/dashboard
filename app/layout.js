import '@/app/styles/globals.css';
import localFont from '@next/font/local';

const SFMono = localFont({
  src: './styles/SF-Mono-Regular.otf',
  variable: '--font-SFMono',
});

export default function RootLayout({ children }) {
  return (
    <html className={'bg-black'}>
      <head>
        <title>{'.'}</title>
        <meta content='width=device-width, initial-scale=1' name='viewport' />
        <link rel='icon' href='/app/styles/favicon.ico' />
        <meta name='robots' content='noindex' />
      </head>
      <body className={SFMono.variable}>{children}</body>
    </html>
  );
}
