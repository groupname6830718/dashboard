'use client';
import { GridToolbarContainer } from '@mui/x-data-grid-premium';
import { useContext, useState } from 'react';
import IssuingButton from '@/app/DataGrid/slots/toolbar/IssuingButton';
import FilterButton from '@/app/DataGrid/slots/toolbar/FilterButton';
import ExportButton from '@/app/DataGrid/slots/toolbar/ExportButton';
import ObjectsButton from '@/app/DataGrid/slots/toolbar/ObjectsButton';
import { DetailPanelActionContext } from '@/app/DataGrid/context/DetailPanelActionContext';
import { SetDetailPanelActionContext } from '@/app/DataGrid/context/SetDetailPanelActionContext';
import ColumnsButton from '@/app/DataGrid/slots/toolbar/ColumnsButton';

export default function PaymentsToolbar(props) {
  const [showHidden, setShowHidden] = useState(false);
  const [filterMenuOpen, setFilterMenuOpen] = useState(false);
  const [columnMenuOpen, setColumnMenuOpen] = useState(false);
  const [exportMenuOpen, setExportMenuOpen] = useState(false);

  const detailPanelAction = useContext(DetailPanelActionContext);
  const setDetailPanelAction = useContext(SetDetailPanelActionContext);
  function checkToolbarButtonPress(x, y) {
    const toolbarButtons = [
      'objectsButton2',
      'filterButton2',
      'columnsButton2',
      'exportButton2',
      'issuingButton2',
    ];

    let foundButton = false;
    for (let buttonID in toolbarButtons) {
      const buttonElement = document.getElementById(toolbarButtons[buttonID]);

      if (buttonElement) {
        const rect = buttonElement.getBoundingClientRect();
        const id = buttonElement.id;

        if (x >= rect.x && x <= rect.x + rect.width) {
          if (y >= rect.y && y <= rect.y + rect.height) {
            if (id === 'objectsButton2') {
              setDetailPanelAction(
                detailPanelAction === 'objects' ? 'payments' : 'objects'
              );
              foundButton = true;
            } else if (id === 'filterButton') {
              setFilterMenuOpen(true);
              if (detailPanelAction === 'payments') {
                setShowHidden(false);
              }
              foundButton = true;
            } else if (id === 'columnsButton') {
              if (detailPanelAction === 'payments') {
                setShowHidden(false);
              }
              setColumnMenuOpen(true);
              foundButton = true;
            } else if (id === 'exportButton2') {
              setExportMenuOpen(true);
              foundButton = true;
            } else if (id === 'issuingButton2') {
              setDetailPanelAction(
                detailPanelAction === 'issuing' ? 'paymnets' : 'issuing'
              );
              foundButton = true;
            }
          }
        }
      }
    }

    if (!foundButton) {
      if (detailPanelAction === 'payments') {
        setShowHidden(false);
      }
    }
  }

  return (
    <GridToolbarContainer>
      <FilterButton
        open={filterMenuOpen}
        setOpen={setFilterMenuOpen}
        data={props.data}
        checkToolbarButtonPress={checkToolbarButtonPress}
      />

      <IssuingButton
        setShowHidden={setShowHidden}
        checkToolbarButtonPress={checkToolbarButtonPress}
      />

      <ColumnsButton
        open={columnMenuOpen}
        setOpen={setColumnMenuOpen}
        setShowHidden={setShowHidden}
        checkToolbarButtonPress={checkToolbarButtonPress}
      />

      {showHidden ? (
        <>
          <ObjectsButton
            setShowHidden={setShowHidden}
            checkToolbarButtonPress={checkToolbarButtonPress}
          />

          <ExportButton
            color={'primary'}
            open={exportMenuOpen}
            setOpen={setExportMenuOpen}
            setShowHidden={setShowHidden}
            checkToolbarButtonPress={checkToolbarButtonPress}
          />
        </>
      ) : (
        <></>
      )}
    </GridToolbarContainer>
  );
}
