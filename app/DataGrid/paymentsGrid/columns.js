import numberWithSpaces from '@/app/DataGrid/helperFunctions/numberWithSpaces';
import getPlaceName from '@/app/DataGrid/helperFunctions/getPlaceName';

export function getColumns(places) {
  return [
    {
      field: 'place',
      headerName: 'name',
      valueFormatter: (params) => getPlaceName(params, places),
      width: 168,
    },
    {
      field: 'amount',
      width: 90,
      type: 'number',
      valueFormatter: (x) => numberWithSpaces(x.value),
    },
    {
      field: 'name',
      headerName: 'payment',
      width: 90,
      headerAlign: 'right',
      align: 'right',
    },
    {
      field: 'status',
      width: 98,
    },
    {
      field: 'processing',
      width: 150,
    },
    {
      field: 'payment_failed',
      headerName: 'payment failed',
      width: 150,
    },
    {
      field: 'added',
      width: 150,
    },
    {
      field: 'succeeded',
      width: 150,
    },
    {
      field: 'requires_payment_method',
      headerName: 'requries payment method',
      width: 150,
    },
    {
      field: 'complete',
      width: 150,
    },
    {
      field: 'id',
      width: 150,
    },
  ];
}
