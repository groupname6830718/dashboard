export const initialState = {
  columns: {
    columnVisibilityModel: {
      id: false,
      __check__: false,
      __reorder__: false,
      __detail_panel_toggle__: true,
      payment: false,
      processing: false,
      payment_failed: false,
      added: false,
      succeeded: false,
      requires_payment_method: false,
      complete: false,
    },
  },
  aggregation: {
    model: {
      amount: 'sum',
    },
  },
};
