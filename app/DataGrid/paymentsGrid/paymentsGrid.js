'use client';
import { useState, useContext, useEffect } from 'react';
import { StyledGrid } from '@/app/DataGrid/StyledGrid';
import SearchIcon from '@mui/icons-material/Search';
import { getColumns } from '@/app/DataGrid/paymentsGrid/columns';
import { initialState } from '@/app/DataGrid/paymentsGrid/initialState';
import PaymentsToolbar from '@/app/DataGrid/paymentsGrid/PaymentsToolbar';
import { PlacesGridRowsContext } from '@/app/PlacesGridRowsContext';
import { PaymentsGridRowsContext } from '@/app/PaymentsGridRowsContext';
import { SetDetailPanelActionContext } from '@/app/DataGrid/context/SetDetailPanelActionContext';
import { DetailPanelActionContext } from '@/app/DataGrid/context/DetailPanelActionContext';
import TimelineIcon from '@mui/icons-material/Timeline';
import MinimizeIcon from '@mui/icons-material/Minimize';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import HorizontalRuleIcon from '@mui/icons-material/HorizontalRule';
import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ViewInArIcon from '@mui/icons-material/ViewInAr';
import ObjectsDetail from '@/app/DataGrid/slots/detailPanel/ObjectsDetail';
import PaymentDetail from '@/app/DataGrid/slots/detailPanel/PaymentDetail';

export default function PaymentsGrid(props) {
  const places = useContext(PlacesGridRowsContext);
  const payments = useContext(PaymentsGridRowsContext);
  const [detailPanelAction, setDetailPanelAction] = useState('payments');
  const [detailPanelIcon, setDetailPanelIcon] = useState(TimelineIcon);

  useEffect(() => {
    if (detailPanelAction === 'objects') {
      setDetailPanelIcon(ViewInArIcon);
    } else {
      setDetailPanelAction(TimelineIcon);
    }
  }, [detailPanelAction]);

  function getDetailPanelContent(x) {
    if (detailPanelAction === 'payments') return <PaymentDetail data={x} />;
    else if (detailPanelAction === 'objects') return <ObjectsDetail data={x} />;
  }
  return (
    <DetailPanelActionContext.Provider value={detailPanelAction}>
      <SetDetailPanelActionContext.Provider value={setDetailPanelAction}>
        <StyledGrid
          rows={payments}
          columns={getColumns(props.initialPlaces)}
          initialState={initialState}
          getDetailPanelContent={getDetailPanelContent}
          slots={{
            detailPanelExpandIcon: detailPanelIcon,
            detailPanelCollapseIcon: MinimizeIcon,
            columnMenuIcon: MoreVertIcon,
            columnUnsortedIcon: HorizontalRuleIcon,
            columnSortedAscendingIcon: ExpandLessIcon,
            columnSortedDescendingIcon: ExpandMoreIcon,
            columnFilteredIcon: SearchIcon,
            toolbar: PaymentsToolbar,
          }}
          getDetailPanelHeight={() => 'auto'}
          checkboxSelection
          density='compact'
          hideFooter
          slotProps={{
            toolbar: { currentGridState: props.currentGridState },
          }}
          experimentalFeatures={{ aggregation: true }}
          {...props}
          sx={{
            height: '100vh',
          }}
        />
      </SetDetailPanelActionContext.Provider>
    </DetailPanelActionContext.Provider>
  );
}
