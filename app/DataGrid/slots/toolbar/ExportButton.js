'use client';
import IconButton from '@mui/material/IconButton';
import { Button, Menu } from '@mui/material';
import Fade from '@mui/material/Fade';
import { useGridApiContext } from '@mui/x-data-grid-premium';
import SdCardOutlinedIcon from '@mui/icons-material/SdCardOutlined';
import { DetailPanelActionContext } from '@/app/DataGrid/context/DetailPanelActionContext';
import { useContext, useState } from 'react';

export default function ExportButton(props) {
  const [id] = useState(Math.random().toString());

  const apiRef = useGridApiContext();
  const detailPanelAction = useContext(DetailPanelActionContext);

  const handleClose = (event) => {
    props.checkToolbarButtonPress(event.clientX, event.clientY);
    props.setOpen(false);
    if (detailPanelAction === 'places') props.setShowHidden(false);
  };

  const handleOpen = () => {
    props.setOpen(true);
  };

  return (
    <>
      <IconButton id={id} onClick={handleOpen}>
        <SdCardOutlinedIcon />
      </IconButton>

      <Menu
        anchorEl={document.getElementById(id)}
        open={props.open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        <Button
          size='small'
          variant='text'
          onClick={() => apiRef.current.exportDataAsCsv()}
          disableRipple
        >
          .csv
        </Button>
        <Button
          size='small'
          variant='text'
          onClick={() => apiRef.current.exportDataAsExcel()}
          disableRipple
        >
          .xlsx
        </Button>
      </Menu>
    </>
  );
}
