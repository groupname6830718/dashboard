'use client';
import IconButton from '@mui/material/IconButton';
import MoreHorizOutlinedIcon from '@mui/icons-material/MoreHorizOutlined';
import { Menu } from '@mui/material';
import Fade from '@mui/material/Fade';
import { GridColumnsPanel } from '@mui/x-data-grid-premium';
import { useContext, useState } from 'react';
import { DetailPanelActionContext } from '@/app/DataGrid/context/DetailPanelActionContext';

export default function ColumnsButton2(props) {
  const [id] = useState(Math.random().toString());

  const detailPanelAction = useContext(DetailPanelActionContext);

  const handleClose = (event) => {
    props.checkToolbarButtonPress(event.clientX, event.clientY);
    props.setOpen(false);
  };

  const handleOpen = () => {
    props.setOpen(true);
    if (detailPanelAction === 'places') {
      props.setShowHidden(true);
    }
  };

  return (
    <>
      <IconButton id={id} onClick={handleOpen}>
        <MoreHorizOutlinedIcon />
      </IconButton>
      <Menu
        anchorEl={document.getElementById(id)}
        open={props.open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        <GridColumnsPanel />
      </Menu>
    </>
  );
}
