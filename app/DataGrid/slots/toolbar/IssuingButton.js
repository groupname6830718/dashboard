'use client';
import IconButton from '@mui/material/IconButton';
import { useState } from 'react';
import AllInclusiveOutlinedIcon from '@mui/icons-material/AllInclusiveOutlined';

export default function IssuingButton(props) {
  const [active, setActive] = useState(false);
  function handleClick() {
    setActive(!active);
  }

  return (
    <>
      <IconButton color='secondary' id='issuingButton2' onClick={handleClick}>
        <AllInclusiveOutlinedIcon
          className={active ? 'selected' : ''}
          id='issuingButtonSVG2'
        />
      </IconButton>
    </>
  );
}
