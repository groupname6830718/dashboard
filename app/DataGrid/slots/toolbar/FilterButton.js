'use client';
import IconButton from '@mui/material/IconButton';
import { Menu } from '@mui/material';
import Fade from '@mui/material/Fade';
import { useGridApiContext, GridFilterPanel } from '@mui/x-data-grid-premium';
import Filter9PlusOutlinedIcon from '@mui/icons-material/Filter9PlusOutlined';
import Filter9OutlinedIcon from '@mui/icons-material/Filter9Outlined';
import Filter8OutlinedIcon from '@mui/icons-material/Filter8Outlined';
import Filter7OutlinedIcon from '@mui/icons-material/Filter7Outlined';
import Filter6OutlinedIcon from '@mui/icons-material/Filter6Outlined';
import Filter5OutlinedIcon from '@mui/icons-material/Filter5Outlined';
import Filter4OutlinedIcon from '@mui/icons-material/Filter4Outlined';
import Filter3OutlinedIcon from '@mui/icons-material/Filter3Outlined';
import Filter2OutlinedIcon from '@mui/icons-material/Filter2Outlined';
import Filter1OutlinedIcon from '@mui/icons-material/Filter1Outlined';
import FilterNoneOutlinedIcon from '@mui/icons-material/FilterNoneOutlined';
import { useEffect, useState } from 'react';

export default function FilterButton(props) {
  const [id] = useState(Math.random().toString());

  const apiRef = useGridApiContext();
  const [activeFilters, setActiveFilters] = useState(
    apiRef.current.exportState().filter.filterModel.items.length
  );

  const handleClose = (event) => {
    props.checkToolbarButtonPress(event.clientX, event.clientY);
    props.setOpen(false);
  };

  const handleOpen = () => {
    props.setOpen(true);
  };

  const [expandedCols, setExpandedCols] = useState([]);

  useEffect(() => {
    resizeFilteredColumns(
      apiRef.current.exportState().filter.filterModel.items
    );

    return apiRef.current.subscribeEvent('filterModelChange', (event) => {
      const items = event.items;
      updateActiveFilters(items);
      resizeFilteredColumns(items);
    });
  }, [apiRef]);

  function resizeFilteredColumns(items) {
    for (let elem in items) {
      if (expandedCols.indexOf(items[elem].field) === -1) {
        const width = apiRef.current.getColumn(items[elem].field).width;
        const field = items[elem].field;
        if (width < 140) {
          apiRef.current.setColumnWidth(field, width + 61);
          setExpandedCols([...expandedCols, items[elem].field]);
        }
      }
    }

    for (let elem in expandedCols) {
      let found = false;
      for (let elem in items) {
        const field = items[elem].field;

        if (field === expandedCols[elem]) {
          found = true;
        }
      }
      if (!found) {
        const width = apiRef.current.getColumn(items[elem].field).width;
        const field = items[elem].field;
        apiRef.current.setColumnWidth(field, width - 61);
        let n = [];
        for (let elem in expandedCols) {
          if (expandedCols[elem] !== field) {
            n.push(field);
          }
        }
        setExpandedCols(n);
      }
    }
  }

  function updateActiveFilters(items) {
    let count = 0;
    for (let elem in items) {
      if (items[elem].value) {
        count++;
      }
    }
    setActiveFilters(count);
  }

  return (
    <>
      <IconButton id={id} onClick={handleOpen}>
        {filterIcons0to9[activeFilters] === 'undefined' ? (
          <Filter9PlusOutlinedIcon />
        ) : (
          filterIcons0to9[activeFilters]
        )}
      </IconButton>
      <Menu
        anchorEl={document.getElementById(id)}
        open={props.open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        <GridFilterPanel />
      </Menu>
    </>
  );
}

export const filterIcons0to9 = {
  0: <FilterNoneOutlinedIcon />,
  1: <Filter1OutlinedIcon />,
  2: <Filter2OutlinedIcon />,
  3: <Filter3OutlinedIcon />,
  4: <Filter4OutlinedIcon />,
  5: <Filter5OutlinedIcon />,
  6: <Filter6OutlinedIcon />,
  7: <Filter7OutlinedIcon />,
  8: <Filter8OutlinedIcon />,
  9: <Filter9OutlinedIcon />,
};
