'use client';
import IconButton from '@mui/material/IconButton';
import ViewInArIcon from '@mui/icons-material/ViewInAr';
import { useContext } from 'react';
import { DetailPanelActionContext } from '@/app/DataGrid/context/DetailPanelActionContext';
import { SetDetailPanelActionContext } from '@/app/DataGrid/context/SetDetailPanelActionContext';

export default function ObjectsButton(props) {
  const detailPanelAction = useContext(DetailPanelActionContext);
  const setDetailPanelAction = useContext(SetDetailPanelActionContext);

  function handleClick() {
    if (detailPanelAction === 'objects') {
      setDetailPanelAction('places');
      props.setShowHidden(false);
    } else {
      setDetailPanelAction('objects');
    }
  }

  return (
    <>
      <IconButton color='secondary' id='objectsButton' onClick={handleClick}>
        <ViewInArIcon id='objectsButtonSVG' />
      </IconButton>
    </>
  );
}
