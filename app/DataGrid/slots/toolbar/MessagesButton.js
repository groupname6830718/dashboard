'use client';
import IconButton from '@mui/material/IconButton';
import DataObjectIcon from '@mui/icons-material/DataObject';
import { useContext } from 'react';
import { DetailPanelActionContext } from '@/app/DataGrid/context/DetailPanelActionContext';
import { SetDetailPanelActionContext } from '@/app/DataGrid/context/SetDetailPanelActionContext';

export default function MessagesButton(props) {
  const detailPanelAction = useContext(DetailPanelActionContext);
  const setDetailPanelAction = useContext(SetDetailPanelActionContext);

  function handleClick() {
    if (detailPanelAction === 'messages') {
      setDetailPanelAction('places');
      props.setShowHidden(false);
    } else {
      setDetailPanelAction('messages');
    }
  }

  return (
    <>
      <IconButton id='messagesButton' onClick={handleClick}>
        <DataObjectIcon id='messagesButtonSVG' />
      </IconButton>
    </>
  );
}
