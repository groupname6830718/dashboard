'use client';
import IconButton from '@mui/material/IconButton';
import InputIcon from '@mui/icons-material/Input';
import { useContext } from 'react';
import { DetailPanelActionContext } from '@/app/DataGrid/context/DetailPanelActionContext';
import { SetDetailPanelActionContext } from '@/app/DataGrid/context/SetDetailPanelActionContext';

export default function AccessControlButton(props) {
  const detailPanelAction = useContext(DetailPanelActionContext);
  const setDetailPanelAction = useContext(SetDetailPanelActionContext);

  function handleClick() {
    if (detailPanelAction === 'accessControl') {
      setDetailPanelAction('places');
      props.setShowHidden(false);
    } else {
      setDetailPanelAction('accessControl');
    }
  }

  return (
    <>
      <IconButton id='accessControlButton' onClick={handleClick}>
        <InputIcon id='accessControlButtonSVG' />
      </IconButton>
    </>
  );
}
