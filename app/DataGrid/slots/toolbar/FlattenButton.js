'use client';
import IconButton from '@mui/material/IconButton';
import ReorderIcon from '@mui/icons-material/Reorder';

export default function FlattenButton() {
  return (
    <>
      <IconButton id='flattenButton'>
        <ReorderIcon id='flattenButtonSVG' />
      </IconButton>
    </>
  );
}
