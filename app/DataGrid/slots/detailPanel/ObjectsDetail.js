import { StyledGrid } from '@/app/DataGrid/StyledGrid';

export default function ObjectsDetail() {
  return (
    <StyledGrid
      rows={[]}
      columns={[]}
      sx={{
        height: '75vh',
      }}
    />
  );
}
