'use client';
import Script from 'next/script';

const jwt =
  'eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjNENTdCMzk1OVYifQ.eyJpc3MiOiJRUzhTM01LVTZMIiwiaWF0IjoxNjcwNTk5MDM5LCJleHAiOjE3MDE5OTM2MDB9.t9EboeZCMHbQsQDRaRWEGP-hstGYzgt-AOWhg4lrOItDSzLrOoej3-HJ2_SvmiaHpYQXiVA01WAHHcIa5_v_KA';

export default function SatelliteMap({ data }) {
  async function main() {
    console.log('data: ');
    console.log(data);
    await setupMapkitJS();
    const map = new mapkit.Map('map-container', {
      region: new mapkit.CoordinateRegion(
        new mapkit.Coordinate(data.row.coordinates[0], data.row.coordinates[1]),
        new mapkit.CoordinateSpan(
          window.innerHeight * 0.00025,
          window.innerWidth * 0.000675
        )
      ),
      mapType: mapkit.Map.MapTypes.Hybrid,
    });
    const propertyAnnotation = new mapkit.MarkerAnnotation(
      new mapkit.Coordinate(data.row.coordinates[0], data.row.coordinates[1])
    );
    propertyAnnotation.color = '#000000';
    propertyAnnotation.selected = 'true';
    propertyAnnotation.glyphText = ' ';
    map.addItems([propertyAnnotation]);
  }
  async function setupMapkitJS() {
    if (!window.mapkit || window.mapkit.loadedLibraries.length === 0) {
      await new Promise((resolve) => {
        window.initMapKit = resolve;
      });
      delete window.initMapKit;
    }
    mapkit.init({
      authorizationCallback: (done) => {
        done(jwt);
      },
    });
  }
  return (
    <div id='map-container' style={{ width: '100vw', height: '85vh' }}>
      <Script
        src='https://cdn.apple-mapkit.com/mk/5.x.x/mapkit.core.js'
        crossorigin
        async
        data-callback='initMapKit'
        data-libraries='full-map'
        data-initial-token={jwt}
        onReady={() => main()}
      />
    </div>
  );
}
