import { createContext } from 'react';

export const SetPlacesSelectedContext = createContext(null);
