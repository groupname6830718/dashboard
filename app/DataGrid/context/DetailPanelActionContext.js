import { createContext } from 'react';

export const DetailPanelActionContext = createContext(null);
