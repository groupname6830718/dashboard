import { createContext } from 'react';

export const SetDetailPanelActionContext = createContext(null);
