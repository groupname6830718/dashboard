import { styled } from '@mui/material/styles';
import { DataGridPremium } from '@mui/x-data-grid-premium';

export const StyledGrid = styled(DataGridPremium)(({ theme }) => ({
  border: 0,
  width: '100%',
  maxHeight: [
    '-webkit-fill-available',
    '-moz-fill-available',
    '-fill-available',
  ],
  '& .MuiDataGrid-cell, .MuiDataGrid-columnHeaders, .MuiDataGrid-pinnedRows,  .MuiDataGrid-detailPanel, .MuiButtonBase-root,  .MuiInputBase-input, .MuiSvgIcon-root.MuiSvgIcon-fontSizeSmall.MuiDataGrid-booleanCell.css-ptiqhd-MuiSvgIcon-root, .MuiPaper-root.MuiMenu-paper.MuiPaper-elevation.MuiPaper-rounded.MuiPaper-elevation8.MuiPopover-paper.css-qa8avv-MuiPaper-root-MuiMenu-paper-MuiPopover-paper, .MuiButtonBase-root.MuiIconButton-root.MuiIconButton-sizeSmall.css-1pe4mpk-MuiButtonBase-root-MuiIconButton-root, .MuiInputLabel-root, .MuiSvgIcon-root.MuiSvgIcon-fontSizeSmall.MuiDataGrid-booleanCell.css-1k33q0, .MuiDataGrid-footerCell.MuiBox-root.css-k6w1hd-MuiDataGrid-footerCell6, .MuiDataGrid-footerCell.MuiBox-root.css-9oshql, .MuiSvgIcon-root.MuiSvgIcon-fontSizeSmall.MuiDataGrid-booleanCell.css-1k33q06':
    {
      // hides row border lines
      borderBottom: 'none',
      // colors cells white
      color: 'white !important',
      backgroundColor: 'black',
    },
  '& .css-242qj4-MuiDataGrid-aggregationColumnHeaderLabel, .MuiDataGrid-aggregationColumnHeaderLabel.css-19dq90j':
    {
      color: 'rgb(255, 255, 255) !important',
    },
  '& .MuiDataGrid-pinnedRows--bottom button, .MuiDataGrid-aggregationColumnHeaderLabel.css-tat99x-MuiDataGrid-aggregationColumnHeaderLabel': {
    display: 'none !important',
  },
}));
