import { doc, setDoc } from 'firebase/firestore';
import { db } from '@/firebase.config';

export default function processRowUpdate(newRow, oldRow) {
  return new Promise((resolve, reject) => {
    const update = {};
    for (let elem in newRow) {
      if (elem !== 'id') {
        update[elem] = newRow[elem];
      }
    }
    setDoc(doc(db, 'places', newRow.id), update)
      .then(() => resolve(newRow))
      .catch(() => reject(oldRow));
  });
}
