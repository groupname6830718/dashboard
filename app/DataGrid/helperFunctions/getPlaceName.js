export default function getPlaceName(params, places) {
  return !params.value ? '' : places[params.value].name;
}
