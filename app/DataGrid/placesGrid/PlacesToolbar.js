'use client';
import { GridToolbarContainer } from '@mui/x-data-grid-premium';
import ObjectsButton from '../slots/toolbar/ObjectsButton';
import FilterButton from '../slots/toolbar/FilterButton';
import ColumnsButton from '../slots/toolbar/ColumnsButton';
import ExportButton from '../slots/toolbar/ExportButton';
import AccessControlButton from '../slots/toolbar/AccessControlButton';
import { useState } from 'react';
import MessagesButton from '../slots/toolbar/MessagesButton.js';
import { useContext } from 'react';
import { DetailPanelActionContext } from '@/app/DataGrid/context/DetailPanelActionContext';
import { SetDetailPanelActionContext } from '@/app/DataGrid/context/SetDetailPanelActionContext';

export default function PlacesToolbar(props) {
  const [showHidden, setShowHidden] = useState(false);
  const [filterMenuOpen, setFilterMenuOpen] = useState(false);
  const [columnMenuOpen, setColumnMenuOpen] = useState(false);
  const [exportMenuOpen, setExportMenuOpen] = useState(false);

  const detailPanelAction = useContext(DetailPanelActionContext);
  const setDetailPanelAction = useContext(SetDetailPanelActionContext);

  function checkToolbarButtonPress(x, y) {
    const toolbarButtons = [
      'objectsButton',
      'filterButton',
      'columnsButton',
      'exportButton',
      'accessControlButton',
      'messagesButton',
    ];

    let foundButton = false;
    for (let buttonID in toolbarButtons) {
      const buttonElement = document.getElementById(toolbarButtons[buttonID]);

      if (buttonElement) {
        const rect = buttonElement.getBoundingClientRect();
        const id = buttonElement.id;

        if (x >= rect.x && x <= rect.x + rect.width) {
          if (y >= rect.y && y <= rect.y + rect.height) {
            if (id === 'objectsButton') {
              setDetailPanelAction(
                detailPanelAction === 'objects' ? 'places' : 'objects'
              );
              foundButton = true;
            } else if (id === 'filterButton') {
              setFilterMenuOpen(true);
              if (detailPanelAction === 'places') {
                setShowHidden(false);
              }
              foundButton = true;
            } else if (id === 'columnsButton') {
              if (detailPanelAction === 'places') {
                setShowHidden(false);
              }
              setColumnMenuOpen(true);
              foundButton = true;
            } else if (id === 'exportButton') {
              setExportMenuOpen(true);
              foundButton = true;
            } else if (id === 'accessControlButton') {
              setDetailPanelAction(
                detailPanelAction === 'accessControl'
                  ? 'places'
                  : 'accessControl'
              );
              foundButton = true;
            } else if (id === 'messagesButton') {
              setDetailPanelAction(
                detailPanelAction === 'messagesButton' ? 'places' : 'messages'
              );
              foundButton = true;
            }
          }
        }
      }
    }

    if (!foundButton) {
      if (detailPanelAction === 'places') {
        setShowHidden(false);
      }
    }
  }

  return (
    <GridToolbarContainer>
      <FilterButton
        open={filterMenuOpen}
        setOpen={setFilterMenuOpen}
        data={props.data}
        checkToolbarButtonPress={checkToolbarButtonPress}
      />
      <ColumnsButton
        open={columnMenuOpen}
        setOpen={setColumnMenuOpen}
        setShowHidden={setShowHidden}
        checkToolbarButtonPress={checkToolbarButtonPress}
      />

      {showHidden ? (
        <>
          <ObjectsButton
            setShowHidden={setShowHidden}
            checkToolbarButtonPress={checkToolbarButtonPress}
          />

          <MessagesButton
            setShowHidden={setShowHidden}
            checkToolbarButtonPress={checkToolbarButtonPress}
          />

          <AccessControlButton
            setShowHidden={setShowHidden}
            checkToolbarButtonPress={checkToolbarButtonPress}
          />

          <ExportButton
            color={'primary'}
            open={exportMenuOpen}
            setOpen={setExportMenuOpen}
            setShowHidden={setShowHidden}
            checkToolbarButtonPress={checkToolbarButtonPress}
          />
        </>
      ) : (
        <></>
      )}
    </GridToolbarContainer>
  );
}
