export const initialState = {
  columns: {
    columnVisibilityModel: {
      id: false,
      __check__: false,
      __reorder__: false,
      stripe_customer_id: false,
      unit: false,
      url: false,
      coordinates: false,
      under_development: false,
      logins: false,
      autopay_payment_method: false,
      payment_timing: false,
      payment_methods: false,
      applications_open: true,
      primary_email: false,
      autopay: false,
      place: false,
      primaryEmail: false,
      selectedPaymentMethod: false,
      inside: false,
      outside: false,
    },
  },
  aggregation: {
    model: {
      price: 'sum',
      value: 'sum',
    },
  },
  filter: {
    filterModel: {
      items: [
        { field: 'under_development', operator: 'is', value: 'true', id: '' },
      ],
    },
  },
};
