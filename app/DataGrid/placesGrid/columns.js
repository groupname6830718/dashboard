import numberWithSpaces from '@/app/DataGrid/helperFunctions/numberWithSpaces';

export const columns = [
  {
    field: 'name',
    width: 168,
  },
  {
    editable: true,
    field: 'price',
    width: 90,
    type: 'number',
    valueFormatter: (x) => numberWithSpaces(x.value),
  },
  {
    field: 'value',
    editable: true,
    width: 90,
    type: 'number',
    valueFormatter: (x) => numberWithSpaces(x.value),
  },
  {
    field: 'applications_open',
    headerName: 'vacant',
    width: 70,
    type: 'boolean',
    editable: true,
  },
  {
    field: 'inside',
    width: 90,
    type: 'number',
    valueFormatter: (x) => numberWithSpaces(x.value),

    editable: true,
  },
  {
    field: 'outside',
    valueFormatter: (x) => numberWithSpaces(x.value),

    width: 90,
    type: 'number',
    editable: true,
  },
  {
    field: 'payment_timing',
    headerName: 'init payment',
    width: 120,
    align: 'center',
    type: 'number',
  },
  {
    field: 'autopay',
    width: 77,
    type: 'boolean',
  },
  {
    field: 'url',
    width: 140,
  },
  {
    field: 'autopay_payment_method',
    headerName: 'autopay payment method',
    width: 0,
    type: 'object',
  },
  {
    field: 'payment_methods',
    headerName: 'payment methods',
    width: 0,
    type: 'object array',
  },
  {
    field: 'logins',
    width: 0,
    type: 'object array',
  },
  {
    field: 'primary_email',
    headerName: 'primary email',
    editable: true,
    width: 125,
  },
  {
    field: 'unit',
    width: 81,
  },
  {
    field: 'stripe_customer_id',
    headerName: 'stripe cust id',
    width: 150,
  },
  {
    field: 'id',
    headerName: 'address',
    width: 300,
  },
  {
    field: 'under_development',
    headerName: 'realized',
    width: 85,
    editable: true,
    type: 'boolean',
  },
];
