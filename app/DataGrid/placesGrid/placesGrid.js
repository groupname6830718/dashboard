'use client';
import { StyledGrid } from '@/app/DataGrid/StyledGrid';
import { useContext } from 'react';
import { initialState } from '@/app/DataGrid/placesGrid/initialState';
import { columns } from '@/app/DataGrid/placesGrid/columns';
import { PlacesGridRowsContext } from '@/app/PlacesGridRowsContext';
import { PlacesSelectedContext } from '@/app/DataGrid/context/PlacesSelectedContext';
import processRowUpdate from '@/app/DataGrid/helperFunctions/processRowUpdate';

export default function PlacesGrid() {
  const placesSelectedStateObject = useContext(PlacesSelectedContext);
  const places = useContext(PlacesGridRowsContext);

  return (

        <StyledGrid
          sx={{
            height: '100vh',
          }}
          rows={places}
          columns={columns}
          initialState={initialState}

          processRowUpdate={processRowUpdate}
          checkboxSelection
          density='compact'
          hideFooter
          experimentalFeatures={{ aggregation: true, newEditingApi: true }}
          disableRowSelectionOnClick
          onRowSelectionModelChange={(x) =>
            placesSelectedStateObject.setPlacesSelected(x)
          }
        />

  );
}
