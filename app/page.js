import ContextProvider from '@/app/ContextProvider';
import PlacesGrid from '@/app/DataGrid/placesGrid/placesGrid';
import PaymentsGrid from '@/app/DataGrid/paymentsGrid/paymentsGrid';
import objectToRows from '@/app/DataGrid/helperFunctions/objectToRows';
import React, { Suspense } from 'react';

export default async function Page() {
  const initialPlacesGridRows = objectToRows(
    await fetch('https://www.units-server.com/api/places')
    .then((res) => res.json())
    .then((data) => {
      return Object.values(data).map((place) => ({
        name: place.name,
        price: place.price,
        value: place.value,
        applications_open: place.applications_open
      }));
    })
    );
  
  
  const initialPlaces = await fetch(
    'https://www.units-server.com/api/places'
  ).then((x) => x.json());

  const initialPaymentsGridRows = objectToRows(
    await fetch('https://www.units-server.com/api/payments').then((x) =>
      x.json()
    )
  );

  return (
    <ContextProvider
      initialPlacesGridRows={initialPlacesGridRows}
      initialPaymentsGridRows={initialPaymentsGridRows}
    >
      <PlacesGrid />
      <Suspense fallback={<div>Loading Payments...</div>}>
        <PaymentsGrid initialPlaces={initialPlaces} />
      </Suspense>
    </ContextProvider>
  );
}
