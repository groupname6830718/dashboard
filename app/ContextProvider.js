'use client';
import { useState, useEffect } from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { MUITheme } from '@/app/styles/MUITheme';
import { onSnapshot, collection, query, orderBy } from 'firebase/firestore';
import { db } from '@/firebase.config';
import { PlacesGridRowsContext } from '@/app/PlacesGridRowsContext';
import { PaymentsGridRowsContext } from '@/app/PaymentsGridRowsContext';
import { PlacesSelectedContext } from '@/app/DataGrid/context/PlacesSelectedContext';
import { SetPlacesSelectedContext } from '@/app/DataGrid/context/SetPlacesSelectedContext';
import objectToRows from '@/app/DataGrid/helperFunctions/objectToRows';

export default function ContextProvider(props) {
  const [placesGridRows, setPlacesGridRows] = useState(
    props.initialPlacesGridRows
  );
  const [paymentsGridRows, setPaymentsGridRows] = useState(
    props.initialPaymentsGridRows
  );
  const [placesSelected, setPlacesSelected] = useState([]);

  useEffect(() => {
    onSnapshot(
      query(collection(db, 'places'), orderBy('value', 'desc')),
      (snapshot) => {
        const update = {};
        snapshot.forEach((doc) => (update[doc.id] = doc.data()));
        setPlacesGridRows(objectToRows(update));
      }
    );

    onSnapshot(
      query(collection(db, 'payments'), orderBy('added', 'desc')),
      (snapshot) => {
        const update = {};
        snapshot.forEach((doc) => (update[doc.id] = doc.data()));
        setPaymentsGridRows(objectToRows(update));
      }
    );
  }, []);

  return (
    <PlacesGridRowsContext.Provider value={placesGridRows}>
      <PaymentsGridRowsContext.Provider value={paymentsGridRows}>
        <PlacesSelectedContext.Provider value={placesSelected}>
          <SetPlacesSelectedContext.Provider value={setPlacesSelected}>
            <ThemeProvider theme={MUITheme}>{props.children}</ThemeProvider>
          </SetPlacesSelectedContext.Provider>
        </PlacesSelectedContext.Provider>
      </PaymentsGridRowsContext.Provider>
    </PlacesGridRowsContext.Provider>
  );
}
