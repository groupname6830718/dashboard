import { createTheme } from '@mui/material/styles';
export const MUITheme = createTheme({
  palette: {
    primary: {
      main: '#ffffff',
    },
    background: {
      paper: '#000000',
    },
  },
  typography: {
    fontFamily: 'var(--font-SFMono)',
  },
});
